# coding=utf-8
from __future__ import unicode_literals
from pytg.utils import coroutine
from pytg import Telegram

tg = Telegram(
    telegram="/root/tg/bin/telegram-cli",
    pubkey_file="/root/tg/tg-server.pub")


def main():
	receiver = tg.receiver
	sender = tg.sender
	receiver.start()
	# add "example_function" function as message listener. You can supply arguments here (like sender).
	receiver.message(example_function(sender))  # now it will call the example_function and yield the new messages.
	# please, no more messages. (we could stop the the cli too, with sender.safe_quit() )
	receiver.stop()

	# continues here, after exiting while loop in example_function()
	print("I am done!")

	# the sender will disconnect after each send, so there is no need to stop it.
	# if you want to shutdown the telegram cli:
	# sender.safe_quit() # this shuts down the telegram cli.
	# sender.quit() # this shuts down the telegram cli, without waiting for downloads to complete.


# this is the function which will process our incoming messages
@coroutine
def example_function(sender): # name "example_function" and given parameters are defined in main()
	QUIT = False
	ADMIN_ID = 82725741
	try:
		while not QUIT:
			msg = (yield)
			sender.status_online()
			print(msg)
			if msg.event != "message":
				continue
			elif msg.own:
				continue
			elif "text" not in msg:
				continue
			elif msg.receiver.type == "chat":
				reply = str(sender.chat_info(msg.peer.cmd))
				sender.send_msg(msg.sender.cmd, reply)
			else:
				if msg.text == u"ping":
					sender.send_msg(msg.peer.cmd, u"Pöng!")
				elif msg.text == u"quit":
					if msg.sender.id == ADMIN_ID:
						sender.send_msg(msg.sender.cmd, u"Bye!")
						QUIT = True
					else:
						reply = u"You are not my Admin.\nMy Admin has id {admin_id} but you have {user_id}".format(admin_id=ADMIN_ID, user_id=msg.sender.id)
						sender.send_msg(msg.sender.cmd, reply)
	except GeneratorExit:
		pass
	except KeyboardInterrupt:
		pass
	else:
		pass


## program start here ##
if __name__ == '__main__':
	main()	# executing main function.
			# Last command of file (so everything needed is already loaded above)