#!/usr/bin/python
#-*- coding: utf-8 -*-

import urllib.request
import requests

def function(mag, matches, peer):
	urllib.request.urlretrieve("http://thecatapi.com/api/images/get?api_key=MjYwMTg&type=gif", "image.gif")
	peer.send_document("image.gif")

plugin = {
	'name': "Catgif",
	'tag': "catgif",
	'patterns': ["^/(catgif)$"],
	'function': function,
	'elevated': True, 
	'usage': "/catgif",
	'desc': "Returns a random cat gif"
	}

