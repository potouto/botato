import requests

class Bot:
	url = "https://api.telegram.org/bot"
	def __init__(self, token):
		self.url = self.url + token + "/"
	def getUpdates(self, offset = 0, limit = 100, timeout = 0):
		payload = {
			'offset': offset,
			'limit': limit,
			'timeout': timeout
		}
		return requests.get(self.url + "getUpdates", params = payload).json()

	def sendMessage(self, chat_id, text, reply_to_message_id = None):
		payload = {
			'chat_id': chat_id,
			'text': text
		}
		if reply_to_message_id != None:
			payload["reply_to_message_id"] = reply_to_message_id
		return requests.get(self.url + "sendMessage", params = payload).json()
	
	def sendPhoto(self, chat_id, photo, caption = "", reply_to_message_id = None):
		files = {'photo': open(photo, 'rb')}
		payload = {
			'chat_id': chat_id,
			'caption': caption
		}
		if reply_to_message_id != None:
			payload["reply_to_message_id"] = reply_to_message_id

		return requests.post(self.url + "sendPhoto", data=payload, files=files).json()
