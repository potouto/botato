#!/usr/bin/python
#-*- coding: utf-8 -*-

import urllib.request
import requests

def function(mag, matches, peer):
	urllib.urlretrieve("http://thecatapi.com/api/images/get?api_key=MjYwMTg&type=png,jpg", "image.jpg")
	peer.send_photo("image.jpg")

plugin = {
	'name': "Cat",
	'tag': "cat",
	'patterns': ["^/(cat)$"],
	'function': function,
	'elevated': False, 
	'usage': "/cat",
	'desc': "Returns a random cat image"
	}

