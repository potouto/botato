#!/usr/bin/python
#-*- coding: utf-8 -*-

import requests
import json

def function(msg, matches, peer):
	returnText = "Oops, an error occurred."
	r = requests.get("http://www.telize.com/geoip/{}".format(matches[0]))
	if r.text:
		returnText = ""
		a = json.loads(r.text)
		for key, value in a.iteritems():
			returnText = returnText + "{}: {}\n".format(key, value)
	return returnText

plugin = {
	'name': "Ip Info",
	'tag': "",
	'patterns': ["^/ipinfo ([.\d]+)$"],
	'function': function,
	'elevated': False, 
	'usage' : "/ipinfo <ip address>",
	'desc' : "Returns info from an ip address"
	}

