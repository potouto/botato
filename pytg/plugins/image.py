#!/usr/bin/python
#-*- coding: utf-8 -*-

import requests
import urllib
import random

def function(msg, matches, peer):
	phrase = matches[0]# + " filetype:png OR filetype:jpg OR filetype:jpeg"
	payload = {
		'v': "1.0",
		'safe': "active",
		'rsz': "8",
		'q': phrase
	}
	h = requests.get("http://ajax.googleapis.com/ajax/services/search/images", params = payload).json()
	try:
		if len(h["responseData"]["results"]) == 0:
			return "Could not find any images for that search."
		url = random.choice(h["responseData"]["results"])["unescapedUrl"]
		filetype = url.split('.')
		filetype = filetype[len(filetype) - 1]
		urllib.request.urlretrieve(url, "image." + filetype)
		peer.send_photo("image." + filetype)
	except:
		return "Could not find any images for that search."

plugin = {
	'name': "Image",
	'tag': "img",
	'patterns': ["^/i(?:mg)? (.*?)$"],
	'function': function,
	'elevated': False, 
	'usage' : "/define <term>",
	'desc' : "Defines a term"
	}

