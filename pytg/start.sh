if [ $# -eq 0 ]; then
	echo "No arguments provided"
	exit 1
fi

while test $# -gt 0; do
	case "$1" in
		-p)
			shift
			if test $# -gt 0; then
				PROFILE=$1
			else
				echo "no profile specified"
				exit 1
			fi
			shift
			;;
		*)
			break
			;;
	esac
done

/root/tg/bin/telegram-cli -k /root/tg/tg-server.pub -Z /root/potouto-pybot/bot.py -p $PROFILE